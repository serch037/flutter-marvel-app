import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:crypto/crypto.dart';

class MCUCharacter {
  int id;
  String name;
  String description;
  String thumbnail_url;
  int comics;
  int series;
  int stories;
  int events;

  MCUCharacter(this.id, this.name, this.description, this.thumbnail_url,
      this.comics, this.series, this.stories, this.events);

  MCUCharacter.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        description = json['description'],
        thumbnail_url =
            json['thumbnail']['path'] + '.' + json['thumbnail']['extension'],
        comics = json['comics']['available'],
        series = json['series']['available'],
        stories = json['stories']['available'],
        events = json['events']['available'];
}

Future getMCUCharacters() async {
  List<MCUCharacter> characters =  new List();
  final public_key = '';
  final private_key = '';
  final timestamp = DateTime.now().millisecondsSinceEpoch.toString();
  final hash = md5.convert(utf8.encode(timestamp+private_key+public_key)).toString();
  final base_url = 'gateway.marvel.com';
  final  path_url = '/v1/public/characters';
  var offset = '0';
  var limit = '10';
  final _params = {'ts':timestamp,'apikey': public_key, 'hash':hash, 'offset':offset, 'limit':limit};
  final uri = Uri.https(base_url,path_url,_params);
  print(uri.toString());
  var response = await http.get(uri);
  if (response.statusCode == 200) {
    print("success");
    var data = json.decode(response.body);
    var unparsed = data["data"]["results"];
    for (var char in unparsed) {
      characters.add(MCUCharacter.fromJson(char));
    }
    print(unparsed);
  }
  return characters;
}

Future<void> main() async {
  var tmp = await getMCUCharacters();
  print(tmp);
}
