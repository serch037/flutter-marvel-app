import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: Colors.blue,
        ),
        home: MCUList());
  }
}

class MCUCharacter {
  int id;
  String name;
  String description;
  String thumbnail_url;
  int comics;
  int series;
  int stories;
  int events;

  MCUCharacter(this.id, this.name, this.description, this.thumbnail_url,
      this.comics, this.series, this.stories, this.events);

  MCUCharacter.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        description = json['description'],
        thumbnail_url =
            json['thumbnail']['path'] + '.' + json['thumbnail']['extension'],
        comics = json['comics']['available'],
        series = json['series']['available'],
        stories = json['stories']['available'],
        events = json['events']['available'];
}

class MCUList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MCUListState();
}

class MCUListState extends State<MCUList> {
  var page = 0;
  final limit = 9;

  Future getMCUCharacters() async {
    List<MCUCharacter> characters = new List();
    final public_key = '';
    final private_key = '';
    final timestamp = DateTime.now().millisecondsSinceEpoch.toString();
    final hash = md5
        .convert(utf8.encode(timestamp + private_key + public_key))
        .toString();
    final base_url = 'gateway.marvel.com';
    final path_url = '/v1/public/characters';
    var limit = this.limit.toString();
    var offset = (page * this.limit).toString();
    final _params = {
      'ts': timestamp,
      'apikey': public_key,
      'hash': hash,
      'offset': offset,
      'limit': limit
    };
    final uri = Uri.https(base_url, path_url, _params);
    var response = await http.get(uri);
    if (response.statusCode == 200) {
      print("success");
      var data = json.decode(response.body);
      var unparsed = data["data"]["results"];
      for (var char in unparsed) {
        characters.add(MCUCharacter.fromJson(char));
      }
    }
    return characters;
  }

  Widget _CharacterTile(MCUCharacter character) {
    return Padding(
        padding: EdgeInsets.all(8),
        child: ListTile(
          leading: CircleAvatar(
              backgroundImage: NetworkImage(character.thumbnail_url),
              child: Text(character.name[0].toUpperCase(),
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 10))),
          title: Text(character.name),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => MCUCharacterPage(
                          character: character,
                        )));
          },
        ));
  }

  void nextPage() {
    setState(() {
      page += 1;
    });
  }

  void prevPage() {
    setState(() {
      page -= 1;
      if (page < 0) page = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("MCU Characters")),
      body: FutureBuilder(
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                    MCUCharacter character = snapshot.data[index];
                    return Column(
                      children: <Widget>[_CharacterTile(character)],
                    );
                  });
            } else {
              return Container(
                child: Text('Loading'),
              );
            }
          },
          future: getMCUCharacters()),
      persistentFooterButtons: <Widget>[
        IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: (page > 0) ? prevPage : null),
        IconButton(icon: Icon(Icons.arrow_forward), onPressed: nextPage),
      ],
    );
  }
}

class MCUCharacterPage extends StatelessWidget {
  final MCUCharacter character;

  const MCUCharacterPage({Key key, this.character}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("MCU Character")),
        body: Container(
            child: Column(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: NetworkImage(character.thumbnail_url),
                      fit: BoxFit.cover)),
              width: double.infinity,
              height: MediaQuery.of(context).size.height / 3,
            ),
            ListTile(
              leading: Text('Name'),
              title: Text(character.name),
            ),
            ListTile(
              leading: Text('Description'),
              title: Text(character.description.toString()),
            ),
            ListTile(
              leading: Text('Comics'),
              title: Text(character.comics.toString()),
            ),
            ListTile(
              leading: Text('Series'),
              title: Text(character.series.toString()),
            ),
            ListTile(
              leading: Text('Stories'),
              title: Text(character.events.toString()),
            ),
          ],
        )));
  }
}
